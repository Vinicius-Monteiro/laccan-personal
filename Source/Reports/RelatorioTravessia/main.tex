\documentclass[12pt]{report}
\usepackage[a4paper]{geometry}
\usepackage{graphicx, wrapfig, subcaption, setspace, booktabs}
\usepackage[T1]{fontenc}
\usepackage[font=small, labelfont=bf]{caption}
\usepackage{fourier}
\usepackage[brazil]{babel}
\usepackage{url, lipsum}
\usepackage{tgbonum}
\usepackage{xcolor}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{siunitx}
\usepackage{times}
\usepackage{pdfpages}
\usepackage{hyperref}

\newcommand{\HRule}[1]{\rule{\linewidth}{#1}}
\onehalfspacing

\begin{document}
\title{ \normalsize \textsc{}
		\\ [2.0cm]
		\HRule{0.5pt} \\
		\large \textbf{{Relatório de travessia de matrizes em R e C}
		\HRule{2pt} \\ [0.5cm]}
		}
\author{
		Vinicius Monteiro Pontes \\ 
		Universidade Federal de Alagoas \\
		LaCCAN }
\maketitle

\begin{center}
\textbf{\large{RESUMO}}

\hrulefill
\end{center}

O experimento consistiu na comparação de métodos de travessia de matrizes nas linguagens de programação R e C.
Para tal, utilizou-se a interface \textit{.Call()} em conjunto com o programa em C, e foram coletadas as médias entre cem (100) execuções dos programas. Considerando que as implementações utilizam os mesmos algoritmos, a comparação da eficiência destas pode se mostrar importante para sistemas que venham a utilizar uma delas.

\newpage
\begin{center}
\textbf{\large{OBJETIVOS DO EXPERIMENTO}}

\hrulefill
\end{center}

Com suporte das linguagens R e C, bem como da interface \textit{.Call()}, encontrar o algoritmo mais eficiente de travessia em matrizes ao analisar os métodos vertical e horizontal.


\newpage
\begin{center}
\textbf{\large{INTRODUÇÃO}}

\hrulefill
\end{center}

Por conta da natureza investigativa do Método Científico, tornam-se essenciais para a Ciência, formas de analisar grandes quantidades de informações de maneira individual, mas eficiente com o suporte da Computação. Matrizes são uma das principais estruturas nas quais se armazenam esses dados e os algoritmos sobre elas utilizados ainda são tema de extensa pesquisa na Ciência da Computação. 

Até o algoritmo mais simples de analisar individualmente elementos de uma matriz - iterando por todos eles de forma sequencial, pode ser feito de maneiras diferentes. O algoritmo consiste em fixar uma linha ou coluna da matriz e percorrer todas as suas colunas ou linhas, respectivamente, incrementando, então, o índice fixado. Apesar de possuírem a mesma ordem de complexidade, as duas formas podem ter diferenças de eficiência, a depender da linguagem utilizada.

Essas diferenças nascem de decisões arquiteturais da linguagem acerca do método de armazenamento de informação, decisões essas, que devem ser tomadas devido à natureza contínua da memória de computadores. As formas que as linguagens aqui abordadas tratam a continuidade de matrizes são “row-major” - elementos consecutivos de uma linha são adjacentes na memória; e “column-major” - o mesmo para elementos de uma mesma coluna. A primeira é utilizada por linguagens como C, C++ e Pascal, enquanto a segunda por linguagens como R, MATLAB e Fortran. Entretanto, são abordadas aqui apenas R e C.

\newpage
\begin{center}
\textbf{\large{DESENVOLVIMENTO DO EXPERIMENTO}}

\hrulefill
\end{center}

Inicialmente, foi decidido que os métodos de ambas as linguagens seriam executados por um único script em R. Nesse, ficariam incluídas as implementações em R e as “chamadas” para as funções em C, que necessitam estar contidas em arquivos separados. Foi, então, criada uma matriz de tamanho 1000 por 1000 preenchida por zeros e esta, percorrida duas vezes. Cada uma das vezes com uso de dois loops \textit{for} aninhados, na primeira, percorrendo todas as colunas de cada linha por vez, representado o método horizontal de travessia; e na segunda, percorrendo todas as linhas de cada coluna, representando o vertical. 

Foi, também, criada uma matriz auxiliar de mesmo tamanho e dentro de cada um dos loops internos, foi atribuído o termo atual da matriz principal a um termo da auxiliar, forçando, então, a execução do programa a indexar as matrizes. Com uma chamada de sistema, foi medido o tempo antes e depois da execução de cada método e obtido o tempo passado de acordo com a diferença entre os dois. 

Os mesmos dois métodos foram, então, implementados em C com uso da interface \textit{.Call()}, passando como parâmetros da função a matriz principal e sua quantidade de linhas e colunas. A matriz secundária, de mesmo tamanho, foi alocada dinamicamente, e o comportamento em R de preencher a matriz com zeros no momento de inicialização foi simulado com uso de \textit{calloc()}. 

Da mesma forma, foi medido o tempo antes e depois da execução dos códigos em C e os tempos de execução das quatro implementações armazenados. Para uma melhor acurácia dos testes, foi coletada a média dos tempos em 100 execuções.

Os programas em C foram compilados com \textit{R CMD SHLIB arquivo.c}, e o programa em R foi executado com \textit{Rscript arquivo.r}. 

\newpage
\begin{center}
\textbf{\large{DISCUSSÃO SOBRE OS RESULTADOS E CONCLUSÃO}}

\hrulefill

\end{center}

\textbf{Hipótese:}

O método de travessia mais eficiente em cada uma das duas linguagens será aquele que percorre a matriz na mesma ordem em que essa é armazenada pela linguagem: horizontalmente, para "row-major" e verticalmente, para "column-major". Assim a diferença de tempo será considerável.

\vspace{3mm}
\textbf{Resultados obtidos:}

Abaixo, está a média dos tempos de execução coletados em cem execuções do programa:

\vspace{2mm}

\begin{center}
\begin{tabular}{|c|c|c|}
     \hline
      & horizontal & vertical\\
      \hline
      R & \SI{0.125388}{\second} & \SI{0.113293}{\second}\\
      \hline
      C & \SI{0.002361}{\second} & \SI{0.020656}{\second}\\
      \hline
\end{tabular}\\

\end{center}

\vspace{10mm}

Para demonstrar a escalabilidade desse padrão, foram também coletados os resultados para matrizes de diferentes tamanhos, 2000 por 2000 e 3000 por 3000. Abaixo seguem os boxplots para efeito de comparação, com o eixo vertical representando o tempo em segundos:


\includepdf[pages={-}]{../../../Data/Rplot.pdf}

\vspace{3mm}

Após análise, percebe-se que os resultados coletados estão em parte alinhados com aqueles esperados, visto que o método horizontal obteve maior eficiência em C, e o vertical em R. Entretanto, em R, o segundo representou uma melhora de em torno de 10\% em relação ao horizontal, em claro contraste à melhora de aproximadamente 90\% do vertical para o horizontal em C.

Fica clara, também, a grande diferença de eficiência do melhor método em C para o melhor em R, por volta de 98\% mais rápido. Isto posto, a grande melhora computacional proporcionada pelo baixo nível de C compensa uma possível perda de abstração e mostra ser de grande proveito o uso de interfaces para linguagens de mais baixo nível em sistemas críticos e/ou que operem sobre grandes quantidades de dados.

\newpage

\nocite{*}

\bibliographystyle{plain}
\bibliography{../../../Bibliography/bibliography.bib}

\end{document}