#include <R.h>
#include <Rinternals.h>
#include <Rmath.h>
#include <stdlib.h>

SEXP rowFirst(SEXP matrix, SEXP rows, SEXP cols) {
	int l = *INTEGER(rows);
	int c = *INTEGER(cols);
	double *m = REAL(matrix);
	
	double **new_matrix = (double**)calloc(l, sizeof(double*));//alocando a matriz dinamicamente
	for(int r = 0; r < l; r++){
		new_matrix[r] = (double*)calloc(c, sizeof(double));//calloc para inicializar tudo como 0, assim como R faz
	}

	for(int i = 0; i < c; i++){
		for(int j = 0; j < l; j++){
			new_matrix[j][i] = m[(j * c) + i];
		}
	}
	
	for(int r = 0; r < l; r++){
		free(new_matrix[r]);
	}
	free(new_matrix);

	return R_NilValue;
}