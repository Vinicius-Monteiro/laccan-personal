#!/usr/bin/env Rscript
library(ggplot2)

colR <- function(rows, cols){
	x <- numeric(100)
	m <- matrix(0, rows, cols)

	print(paste("Starting iteration of column-first R loop"))

	m2 <- matrix(0, rows, cols)

	for(k in 1:length(x)){
		start.time <- Sys.time()
		for(i in 1:nrow(m)){
			for(j in 1:ncol(m)){
				m2[i, j] = m[i, j];
			}
		}
		end.time <- Sys.time()
		x[k] <- (end.time - start.time)
	}
	return (x)
}

rowR <- function(rows, cols){
	x <- numeric(100)
	m <- matrix(0, rows, cols)

	print(paste("Starting iteration of row-first R loop"))

	m2 <- matrix(0, rows, cols)

	for(k in 1:length(x)){
		start.time <- Sys.time()
		for(j in 1:ncol(m)){
			for(i in 1:nrow(m)){
				m2[i, j] = m[i, j];
			}
		}
		end.time <- Sys.time()
		x[k] <- (end.time - start.time)
	}
	return (x)
}

colC <- function(rows, cols){
	#!/usr/bin/env Rscript
	options(scipen=999)#disabling scientific notations
	x <- numeric(100)
	m <- matrix(0, rows, cols)

	print(paste("Starting iteration of column-first C loop"))

	dyn.load("colFirst.so")

	for(i in 1:length(x)){
		start.time <- Sys.time()
		nil <- .Call("colFirst", m, nrow(m), ncol(m))
		end.time <- Sys.time()
		x[i] <- end.time - start.time
	}
	return (x)
}

rowC <- function(rows, cols){
	options(scipen=999)#disabling scientific notations
	x <- numeric(100)
	m <- matrix(0, rows, cols)

	print(paste("Starting iteration of row-first C loop"))

	dyn.load("rowFirst.so")

	for(i in 1:length(x)){
		start.time <- Sys.time()
		nil <- .Call("rowFirst", m, nrow(m), ncol(m))
		end.time <- Sys.time()
		x[i] <- end.time - start.time
	}
	return (x)
}

printInfo <- function(rows, cols, x){
	print(paste("Matrix", rows, "by", cols))
	print(paste("Mean time taken between", length(x), "runs:", mean(x)))
	print(paste("Median time taken between", length(x), "runs:", median(x)))
}

rows <- 1000
cols <- 1000

column.first.r = colR(rows, cols)
printInfo(rows, cols, column.first.r)

row.first.r = rowR(rows, cols)
printInfo(rows, cols, row.first.r)

column.first.c = colC(rows, cols)
printInfo(rows, cols, column.first.c)

row.first.c = rowC(rows, cols)
printInfo(rows, cols, row.first.c)

data <- data.frame(
	"r" = c((column.first.r), (row.first.r)), 
	"c" = c((column.first.c), (row.first.c)),
	stringsAsFactors = FALSE)

#pdf("../../Data/Rplot.pdf")

aux <- data.frame("Horizontal" = data[1:100, "r"], "Vertical" = data[101:200, "r"], stringsAsFactors = FALSE)

boxplot(aux,
	main = "Métodos de travessia em R",
	xlab = paste("Matriz", rows, "por", cols),
	ylab = "Tempo médio de execução",
	append = TRUE)

aux <- data.frame("Horizontal" = data[1:100, "c"], "Vertical" = data[101:200, "c"], stringsAsFactors = FALSE)

boxplot(aux,
	main = "Métodos de travessia em C",
	xlab = paste("Matriz", rows, "por", cols),
	ylab = "Tempo médio de execução",
	append = TRUE)